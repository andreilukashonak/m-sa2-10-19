## 10.Jenkins.Start.md

### Andrei Lukashonak

### How to run 
Starting vagrant:
`
$vagrant up
`

### Screenshots

![](./Jenkins_console.png "Console Output")

![](./Jenkins_workspace.png "GIT Repo")
